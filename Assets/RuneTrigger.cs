using UnityEngine;


public class RuneTrigger : MonoBehaviour
{

    public enum Type
    {
        ResetStroke,
        AutoAim
    }

    public Type type = Type.ResetStroke;
    public GameObject ParticlePrefab;
    public Color color = Color.magenta;
    public float activeTime = 3f;
    public UnityEngine.Rendering.Universal.Light2D accentLight;

    private GameObject active;
    private GameManager _gameManager;

    private void Start()
    {
        active = transform.Find("Active").gameObject;
        if (!active)
        {
            Debug.LogWarning("Active child not set, please check your rune");
        }
        _gameManager = GameManager.Instance;

        if (!accentLight) Debug.LogWarning("No accent light set.", this);
        else accentLight.color = color;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Player") return;

        active.SetActive(false);
        Debug.Log("Rune trigger activated");

        StartCoroutine(_gameManager.ChangePlayerFlameColor(activeTime, color));

        switch (type)
        {
            case Type.ResetStroke:
                _gameManager.ResetCurrentStrokeCount();
                break;
            case Type.AutoAim:
                StartCoroutine(_gameManager.ActivateAutoAim(activeTime));
                break;
            default:
                Debug.LogWarning("Not a valid type specified");
                break;
        }

        Instantiate(ParticlePrefab, transform);
    }

    
}
