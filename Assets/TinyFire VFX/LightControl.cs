﻿using UnityEngine;

public class LightControl : MonoBehaviour {

    float nRand = 0;
	
	void Update ()
    {
        nRand = Random.Range(4f, 5f);
        transform.GetComponent<Light>().intensity = nRand;
	}
}
