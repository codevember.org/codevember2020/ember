using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
namespace UnityTemplateProjects
{
    [CustomEditor(typeof(TemperatureController))]
    public class TemperatureControllerEditor : Editor
    {
        private TemperatureController.ColliderShape _shape;

        public override void OnInspectorGUI()
        {
            var controller = (TemperatureController) target;


            _shape = (TemperatureController.ColliderShape) EditorGUILayout.EnumPopup("Shape", _shape);
            var colliderShape = controller.colliderShape = _shape;

            var value = controller.changeValue = EditorGUILayout.FloatField("Value", controller.changeValue);
            var range = controller.colliderRange = EditorGUILayout.FloatField("Range", controller.colliderRange);
            var cooldown = controller.cooldownValue = EditorGUILayout.FloatField("Cooldown", controller.cooldownValue);
            var consumable = controller.destroyAfterUse =
                EditorGUILayout.Toggle("Consumable", controller.destroyAfterUse);

            EditorGUILayout.Space(14.0f);

            if (GUILayout.Button("Create")) 
                controller.InitCollider(colliderShape, value, range, cooldown, consumable);

            if (GUILayout.Button("Remove Collider")) 
                controller.RemoveCollider();
        }
    }
}
#endif