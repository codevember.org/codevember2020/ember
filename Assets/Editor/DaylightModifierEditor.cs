﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DaylightModifier))]
public class DaylightModifierEditor : Editor
{
    private void OnEnable()
    {
        EditorApplication.update += ExecuteCoroutine;
    }

    public override void OnInspectorGUI()
    {
        DaylightModifier daylightModifierTarget = (DaylightModifier)target;
        DrawDefaultInspector();

        EditorGUILayout.ColorField(daylightModifierTarget.GetCurrentColorForDaylight());
        //myTarget.experience = EditorGUILayout.IntField("Experience", myTarget.experience);
        //EditorGUILayout.GradientField(daylightModifierTarget.DaylightGradient);

        if (GUILayout.Button("Update lights"))
        {
            Undo.RecordObject(daylightModifierTarget, "Update lights 1");
            daylightModifierTarget.UpdateLight();

            // Mark unsaved changes, otherwise they will be dropped on play/exit
            EditorUtility.SetDirty(daylightModifierTarget.GlobalBackgroundLight);
            EditorUtility.SetDirty(daylightModifierTarget.GlobalLight);

            Undo.RecordObject(daylightModifierTarget, "Update lights 2");

            // Redraw everything
            //SceneView.RepaintAll(); // SceneView.currentDrawingSceneView.Repaint();
        }

        if (GUILayout.Button("Simulate a day"))
        {
            StartCoroutine(daylightModifierTarget.SimulateADayCoroutine(20f));
        }
    }

    /// <summary>
    ///  Coroutine to execute. Manage by the EasyLocalization_script
    /// </summary>
    private static List<IEnumerator> CoroutineInProgress = new List<IEnumerator>();
    static int currentExecute = 0;

    // This is my callable function
    public static IEnumerator StartCoroutine(IEnumerator newCorou)
    {
        CoroutineInProgress.Add(newCorou);
        return newCorou;
    }

    private static void ExecuteCoroutine()
    {
        if (CoroutineInProgress.Count <= 0)
        {
            //  Debug.LogWarning("ping");
            return;
        }

        // Debug.LogWarning("exec");

        currentExecute = (currentExecute + 1) % CoroutineInProgress.Count;

        bool finish = !CoroutineInProgress[currentExecute].MoveNext();

        if (finish)
        {
            CoroutineInProgress.RemoveAt(currentExecute);
        }

        SceneView.RepaintAll();
    }
}
