﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using TMPro;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

public class UIManager : MonoBehaviour
{
    [Header("Parameter")]
    [Tooltip("something like `card_03_b`")]
    public string[] introTextKeys;
    [Tooltip("For the scene start and end transitions")]
    public float waitingTimeForContinueButtonInSeconds = 1.5f;
    public float textFadeDurationInSeconds = 1f;

    [Header("Linked Game Objects")]
    public GameObject restartOverlay;
    public GameObject goalOverlay;
    public TextMeshProUGUI lowTemperatureText;
    public TextMeshProUGUI fogText;
    public TextMeshProUGUI levelIntroText;
    public TextMeshProUGUI continueText;

    public Transform minorStrokeContainer;
    public GameObject minorStrokePrefab;

    private int introTextIndex = 0;
    private bool isAllowedToSkipIntroText = false;

    // Listens for clicks/touches
    private void Update()
    {
        if (isAllowedToSkipIntroText && Input.GetMouseButtonDown(0))
        {
            EnableContinueState(false);
            StartCoroutine(NextIntroText());
        }
    }

    // Sets the initial intro text
    public void SetFirstIntroText()
    {
        EnableContinueState(true);

        if (introTextKeys.Length == 0) {
            StartCoroutine(EndOfIntroTextsReached(1f));
            return;
        }

        LookupTranslatedString(introTextKeys[introTextIndex]);
    }

    private void EnableContinueState(bool enable)
    {
        if (enable)
        {
            StartCoroutine(FadeInText(continueText));
        }

        isAllowedToSkipIntroText = enable;
        continueText.gameObject.SetActive(enable);
    }

    // Trys to update to the next intro text
    // ... if there is none, the execute the final steps
    public IEnumerator NextIntroText()
    {
        //Debug.Log("Next intro text...");
        StartCoroutine(FadeOutText(levelIntroText));
        yield return new WaitForSecondsRealtime(textFadeDurationInSeconds);
        introTextIndex++;

        if (introTextIndex == introTextKeys.Length)
        {
            StartCoroutine(EndOfIntroTextsReached(textFadeDurationInSeconds));
            yield break;
        } else
        {
            LookupTranslatedString(introTextKeys[introTextIndex]);
            //Debug.Log("Next intro text... LookupTranslatedString");
        }

        StartCoroutine(FadeInText(levelIntroText));
        yield return new WaitForSecondsRealtime(waitingTimeForContinueButtonInSeconds);
        EnableContinueState(true);
    }

    // Finishes up the next level routine
    public IEnumerator EndOfIntroTextsReached(float waitingTimeInSeconds)
    {
        EnableContinueState(false);
        Debug.Log("End of intro texts reached, proceeding...");

        yield return new WaitForSecondsRealtime(waitingTimeInSeconds);
        GameManager.Instance.ExecuteLoadLevelLogic();
    }

    // Looks up a string in the `Story` table and updates the text field when the result is finished
    private void LookupTranslatedString(string key)
    {
        var op = LocalizationSettings.StringDatabase.GetLocalizedStringAsync("Story", key);
        if (op.IsDone)
            UpdateIntroText(op.Result);
        else
            op.Completed += (o) => UpdateIntroText(op.Result);
    }

    private IEnumerator FadeInText(TextMeshProUGUI text)
    {
        var color = text.color;
        color.a = 0f;
        text.color = color;

        var steps = textFadeDurationInSeconds * 25;
        for (int i = 0; i < steps; i++)
        {
            color.a = 1 - (steps - i) / (float)steps;
            text.color = color;
            yield return new WaitForEndOfFrame();
        }

        color.a = 1f;
        text.color = color;
    }

    private IEnumerator FadeOutText(TextMeshProUGUI text)
    {
        var color = text.color;
        var steps = textFadeDurationInSeconds * 25;
        for (int i = 0; i < steps; i++)
        {
            color.a = (steps - i) / (float)steps;
            text.color = color;
            yield return new WaitForEndOfFrame();
        }

        color.a = 0f;
        text.color = color;
    }

    // The method used for the async lookup call
    private void UpdateIntroText(string text)
    {
        //Debug.Log("Setting text:" + text);
        levelIntroText.text = text;
    }

    public void SetRestartOverlay(bool active)
    {
        restartOverlay.SetActive(active);
    }

    public void SetGoalOverlay(bool active)
    {
        if (active && GameManager.CurrentLevelIndex() == 0)
        {
            GameManager.Instance.StartNextLevel();
        }
        else
        {
            goalOverlay.SetActive(active);
        }
    }

    public void EnableLowTemperatureText(bool enable)
    {
        if (enable != lowTemperatureText.gameObject.activeSelf)
            lowTemperatureText.gameObject.SetActive(enable);
    }

    public void EnableFogText(bool enable)
    {
        if (enable != fogText.gameObject.activeSelf)
            fogText.gameObject.SetActive(enable);
    }

    public bool IsOverlayActive()
    {
        return restartOverlay.activeSelf || goalOverlay.activeSelf;
    }
    
    public void ClearLevelStartText()
    {
        levelIntroText.text = "";
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }

    public void UpdateMinorStrokeCount(int count)
    {
        int difference = count - minorStrokeContainer.childCount;

        if(difference > 0)
        {
            for (int i = 0; i < difference; i++)
            {
                Instantiate(minorStrokePrefab, minorStrokeContainer);
            }
        } else if (difference < 0)
        {
            for (int i = 0; i > difference; i--)
            {
                Destroy(minorStrokeContainer.GetChild(0).gameObject);
            }
        }
    }
}
