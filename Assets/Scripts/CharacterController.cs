﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{
    [Header("Linked Game Objects")]
    public LineRenderer lineRenderer;
    public int lineRendererSteps = 10;
    public LineRenderer inputDotLineRenderer;
    public ParticleSystemForceField forceField;
    public Slider powerSlider;
    public bool isEnabled = true;
    private Camera _currentCamera;
    private bool _currentlyApplyingForce;
    private bool _currentlyTouching;

    private GameManager _gameManager;
    private bool _lastTouchBeforeRelease;

    // Specifies whether the player has started yet
    private bool _launched;
    private Rigidbody2D _playerRigidbody;
    private Vector2 _touchCurrent;

    // Input vars
    private Vector2 _touchDownPosition;
    private Vector2 _touchUpPosition;
    private Coroutine _forceQuitSloMoCoroutine;

    public Slider SloMoSlider;

    private float latestInertiaFactor = 0.8f;

    private void Start()
    {
        _playerRigidbody = GetComponent<Rigidbody2D>();
        lineRenderer.enabled = false;
        inputDotLineRenderer.enabled = false;
        _gameManager = GameManager.Instance;
        _currentCamera = _gameManager.currentCamera;

        // A simple 2 color gradient with a fixed alpha of 1.0f.
        Gradient gradient = new Gradient();
        gradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(Color.white, 0.0f), new GradientColorKey(Color.white, 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(1f, 0.0f), new GradientAlphaKey(0.0f, 1.0f) }
        );
        lineRenderer.colorGradient = gradient;
        lineRenderer.positionCount = lineRendererSteps;
        float width = lineRenderer.startWidth;
        lineRenderer.material.mainTextureScale = new Vector2(1f / width, 1.0f);

        SloMoSlider.gameObject.SetActive(false);
        powerSlider.gameObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.collider.CompareTag("Ground") || GameManager.IsPlayerDead())
            return;
        // TODO Idea : Slowly decrease later on
        Debug.Log("Player hit the ground.");
        _gameManager.PlayerDied();
    }

    private void SlowMoStatus(bool sloMoActive)
    {
        var slowMotionSpeed = sloMoActive ? _gameManager.touchSlowMoFactor : 1.0f;
        _gameManager.UpdateTimeScale(slowMotionSpeed);
    }
    
    private IEnumerator ForceQuitSloMo(float waitForSeconds)
    {
        SloMoSlider.gameObject.SetActive(true);

        var diff = waitForSeconds / 100f;
        for (int i = 0; i < 100; i++)
        {
            SloMoSlider.value = 1f - i / 100f;
            yield return new WaitForSecondsRealtime(diff);
        }

        if (_currentlyTouching)
        {
            ButtonUpMethod();
        }
        SloMoSlider.gameObject.SetActive(false);
    }
    
    private void Update()
    {
        // Display 'flying particles' if no wind is applied
        if (!_currentlyApplyingForce)
        {
            var playerMovement = _playerRigidbody.velocity;
            forceField.directionX = -playerMovement.x / 30f;
            forceField.directionY = -playerMovement.y / 30f;
        }

        if (GameManager.IsGoalAchieved() || GameManager.IsPlayerDead() || !_gameManager.IsAnotherStrokeAvailable())
        {
            _currentlyTouching = false;
            _touchDownPosition = Vector2.zero;
            _touchCurrent = Vector2.zero;
            _touchUpPosition = Vector2.zero;
            lineRenderer.enabled = false;
            SlowMoStatus(false);
            return;
        }

        if (Input.GetMouseButtonDown(0) && isEnabled)
        {
            _currentlyTouching = true;
            _touchDownPosition = Input.mousePosition;
            SlowMoStatus(true);
            if (!_launched)
            {
                _playerRigidbody.AddForce(Vector2.up * 2);
                _launched = true;
            }

            if (_gameManager.maxSloMoTime > 0) 
                _forceQuitSloMoCoroutine = StartCoroutine(ForceQuitSloMo(_gameManager.maxSloMoTime));
        }

        if (Input.GetMouseButton(0))
            _touchCurrent = Input.mousePosition;

        if (Input.GetMouseButtonUp(0))
        {
            ButtonUpMethod();
        }
    }

    private void ButtonUpMethod()
    {
        _touchUpPosition = Input.mousePosition;
        _currentlyTouching = false;
        if (_forceQuitSloMoCoroutine != null)
        {
            SloMoSlider.gameObject.SetActive(false);
            StopCoroutine(_forceQuitSloMoCoroutine);
        }
        inputDotLineRenderer.enabled = false;
        powerSlider.gameObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        if (_currentlyTouching)
        {
            var currentStrokeMultiplier = _gameManager.GetCurrentStrokeMultiplier();
            var strokeVector = CalculateNormalizedStrokeVector(_touchCurrent, _touchDownPosition);
            var targetPosition = _playerRigidbody.transform.position - strokeVector*3*currentStrokeMultiplier;
            
            Vector3 initialVelocity = _playerRigidbody.velocity;
            initialVelocity += strokeVector * (currentStrokeMultiplier * _gameManager.windForcePower);

            var magnitude = initialVelocity.magnitude;
            // Note: You don't wanna change the parameters defined below since everything here and in the Visualize method is an approximation
            latestInertiaFactor = Mathf.Max((magnitude < 10) ? magnitude / 10f : 0.8f, 0.3f); // Note: Pay respect to the Trägheit
            initialVelocity = Vector3.ClampMagnitude(initialVelocity * latestInertiaFactor, _gameManager.clampValueForMagnitude*2); // Note: no clue why times 2 :)
            
            // Visualize the current stroke
            Visualize(initialVelocity);
            lineRenderer.enabled = true;
            powerSlider.gameObject.SetActive(true);
            powerSlider.value = strokeVector.magnitude;

            // Update the user relation dot
            var position = _currentCamera.ScreenToWorldPoint(_touchDownPosition);
            position.z = 1;
            inputDotLineRenderer.SetPosition(0, position);
            inputDotLineRenderer.SetPosition(1, position);
            inputDotLineRenderer.enabled = true;
        } else {
            if (_lastTouchBeforeRelease && !GameManager.IsPlayerDead())
            {
                SlowMoStatus(false);
                lineRenderer.enabled = false;
                
                // Only shoot the player if the force is big enough
                var strokeVector = CalculateNormalizedStrokeVector(_touchUpPosition, _touchDownPosition);
                if (strokeVector.magnitude > 0.1f)
                {
                    _playerRigidbody.gravityScale = _gameManager.normalPlayerGravityScale;

                    StartCoroutine(ApplyWindForce(strokeVector));
                    powerSlider.gameObject.SetActive(false);

                    // Also decrease temperature
                    _gameManager.ModifyTemperatureBy(-10);
                }
            }
        }

        _lastTouchBeforeRelease = _currentlyTouching;
        _playerRigidbody.velocity = 
            Vector3.ClampMagnitude(_playerRigidbody.velocity, _gameManager.clampValueForMagnitude);
    }

    // Applys the wind force with a direction vector with values between 0 and 1
    private IEnumerator ApplyWindForce(Vector3 direction)
    {
        if (Debug.isDebugBuild && (Mathf.Abs(direction.x) > 1 || Mathf.Abs(direction.y) > 1))
        {
            Debug.LogError("Direction must be a value between -1 and 1");
        }

        AudioManager.instance.Play("Swoosh");
        _currentlyApplyingForce = true;
        var currentStrokePower = _gameManager.GetCurrentStrokeMultiplier();
        _gameManager.IncreaseCurrentStrokeCount();

        
        
        // Clamp the current velocity
        _playerRigidbody.velocity = Vector3.ClampMagnitude(_playerRigidbody.velocity, 3f);
        // We can also think of a direction dependent clamping, such as this:
        //if ((Vector3.ClampMagnitude(_playerRigidbody.velocity, 1f) + direction).magnitude < 0.2f)
        //Debug.Log("Clamping velocity, since the player stroke was into another direction.");
        
        // Apply force directly to player
        var currentValue = direction * (currentStrokePower * _gameManager.windForcePower);
        if (_playerRigidbody.gravityScale == _gameManager.normalPlayerGravityScale)
            _playerRigidbody.AddForce(currentValue);
        
        // Apply wind force to particle field
        for (float i = 0f; i < _gameManager.windForceApplicationSteps; i++)
        {
            var currentCos = Mathf.Sin(i / _gameManager.windForceApplicationSteps * Mathf.PI);
            var currentWindValue = direction * (currentCos * currentStrokePower * _gameManager.windForcePower);
            currentValue.z = 0;
            forceField.directionX = direction.x * currentCos * _gameManager.particleForceFieldMultiplier;
            forceField.directionY = direction.y * currentCos * _gameManager.particleForceFieldMultiplier / 2f;
            yield return new WaitForSeconds(0.05f);
        }

        forceField.directionX = 0;
        forceField.directionZ = 0;

        _currentlyApplyingForce = false;
    }

    private Vector3 CalculateNormalizedStrokeVector(Vector2 down, Vector2 current)
    {
        // Calculate the input positions in ratio to the screen size
        down.x /= Screen.width;
        down.y /= Screen.height;
        current.x /= Screen.width;
        current.y /= Screen.height;

        // Calculate the stroke direction itself and clamp it to a given ratio of the screen
        // e.g. a divisionFactor of 20 means that you need to move 100/20=5% of the screen to gain maximum power
        var strokeVector2D = down - current;
        var divisionFactor = 4f;
        strokeVector2D = Vector2.ClampMagnitude(strokeVector2D, 1 / divisionFactor);
        strokeVector2D *= divisionFactor; // "normalize" it
        
        return new Vector3(strokeVector2D.x, strokeVector2D.y, 0);
    }

    void Visualize(Vector2 initialVelocity)
    {
        var lastPosition = Vector3.zero;
        for (int i = 0; i < lineRendererSteps; i++)
        {
            if (!(initialVelocity.magnitude < 10f && i > lineRendererSteps * 0.5f) ||
                !(initialVelocity.magnitude < 15f && i > lineRendererSteps * 0.7f) ||
                !(initialVelocity.magnitude < 20f && i > lineRendererSteps * 0.8f))
            {
                lastPosition = CalculatePositionInTime(initialVelocity, (i / (float)lineRendererSteps)/2f * (0.8f / latestInertiaFactor), i);
            }
                
            lineRenderer.SetPosition(i, lastPosition);
        }
    }

    private Vector3 CalculatePositionInTime(Vector2 initialVelocity, float time, int step)
    {
        Vector3 result = _playerRigidbody.position + initialVelocity * time;
        float sY = (-60f * (Mathf.Abs(_gameManager.normalPlayerGravityScale)) * Mathf.Pow(time, 2)) + (initialVelocity.y * time) + _playerRigidbody.position.y;

        // Extended version with current velocity
        // Vector3 result = _playerRigidbody.position + initialVelocity * time;
        // float sY = (-65f * Mathf.Abs(_gameManager.normalPlayerGravityScale) * Mathf.Pow(time, 2)) + (initialVelocity.y * time) + _playerRigidbody.position.y + _playerRigidbody.velocity.y * time * (float)(lineRendererSteps - step + 1) / 4f;
        
        // Note: only use the curve feature if it is a main stroke
        if (_gameManager.IsNextStrokeAMainStroke())
        {
            result.y = sY;
        }
        
        result.z = 1;
        return result;
    }
}