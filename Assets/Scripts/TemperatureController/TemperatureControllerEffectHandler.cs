using UnityEngine;

namespace UnityTemplateProjects
{
    public class TemperatureControllerEffectHandler : MonoBehaviour
    {
        [Header("Linked Game Objects")]
        public ParticleSystem heatEffect;
        public ParticleSystem coldEffect;

        public ParticleSystem GetHeatEffect()
        {
            return heatEffect;
        }

        public ParticleSystem GetColdEffect()
        {
            return coldEffect;
        }
    }
}