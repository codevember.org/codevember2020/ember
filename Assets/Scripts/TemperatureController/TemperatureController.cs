using UnityEngine;

namespace UnityTemplateProjects
{
    public class TemperatureController : MonoBehaviour
    {
        [Header("Parameter")]
        public float changeValue;
        public float cooldownValue;
        public ColliderShape colliderShape;
        public float colliderRange;
        public bool destroyAfterUse;

        [Header("Linked Game Objects")]
        public new Collider2D collider;
        public ParticleSystem heatEffect;
        public ParticleSystem coldEffect;

        private GameManager _manager;
        private TemperatureHelper _helper;

        private void Awake()
        {
            _manager = GameManager.Instance;
        }
        
        public enum ColliderShape
        {
            Radial,
            LinearHorizontal,
            LinearVertical
        }

        public static bool IsLinearBoxType(ColliderShape colliderShape)
        {
            return colliderShape.Equals(ColliderShape.LinearHorizontal) ||
                colliderShape.Equals(ColliderShape.LinearVertical);
        }

        public void InitCollider(ColliderShape shape, float value, float range, float cooldown, bool consumable)
        {
            colliderShape = shape;
            changeValue = value;
            cooldownValue = cooldown;
            colliderRange = range;
            destroyAfterUse = consumable;

            if (_helper == null) 
                _helper = gameObject.AddComponent<TemperatureHelper>();

            _helper.SetValuesAndBuildCollider(shape, value, range, cooldown, consumable);
        }

        public void RemoveCollider()
        {
            if (gameObject.GetComponent<Collider2D>()) 
                DestroyImmediate(gameObject.GetComponent<Collider2D>());

            if (gameObject.GetComponent<TemperatureHelper>())
                DestroyImmediate(gameObject.GetComponent<TemperatureHelper>());
        }
    }
}