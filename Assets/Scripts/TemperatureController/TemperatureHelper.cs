using System;
using UnityEngine;

namespace UnityTemplateProjects
{
    public class TemperatureHelper : MonoBehaviour
    {
        [Header("Parameter")] 
        public float changeValue;
        public float cooldownValue;
        public TemperatureController.ColliderShape colliderShape;
        public float colliderRange;
        public bool isConsumable;

        [Header("Linked Game Objects")]
        public ParticleSystem heatEffect;
        public ParticleSystem coldEffect;
        public new Collider2D collider;
        private GameManager _manager;

        private float _timer;

        private bool _waitingForCooldown;

        private void Awake()
        {
            _manager = GameManager.Instance;
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!GameManager.IsPlayerDead() && other.transform.CompareTag("Player") && !_waitingForCooldown)
            {
                HandleTemperatureChangeProcess();
                GameManager.Instance.ResetCurrentStrokeCount();
                Debug.Log("Enter Temperature Controller");
            }
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            if (!GameManager.IsPlayerDead() && other.transform.CompareTag("Player") && !_waitingForCooldown)
            {
                HandleTemperatureChangeProcess();
                Debug.Log("Stays in Temperature Controller");
            }
        }

        public void SetValuesAndBuildCollider(TemperatureController.ColliderShape shape, float value, float range,
            float cooldown, bool consumable)
        {
            colliderShape = shape;
            changeValue = value;
            colliderRange = range;
            cooldownValue = cooldown;
            isConsumable = consumable;

            BuildCollider();
            SetEffects();
        }

        private void BuildCollider()
        {
            if (gameObject.GetComponent<Collider2D>()) 
                DestroyImmediate(gameObject.GetComponent<Collider2D>());

            Vector2 size;

            if (TemperatureController.IsLinearBoxType(colliderShape))
            {
                collider = gameObject.AddComponent<BoxCollider2D>();
                var boxCollider = collider.GetComponent<BoxCollider2D>();
                size = boxCollider.size;
                size.x = colliderRange;
                size.y = 1;
            } else {
                switch (colliderShape)
                {
                    case TemperatureController.ColliderShape.Radial:
                        collider = gameObject.AddComponent<CircleCollider2D>();
                        collider.GetComponent<CircleCollider2D>().radius = colliderRange;
                        break;
                    case TemperatureController.ColliderShape.LinearHorizontal:
                        break;
                    case TemperatureController.ColliderShape.LinearVertical:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            collider.isTrigger = true;
        }

        private void SetEffects()
        {
            var effects = GetComponentInChildren<TemperatureControllerEffectHandler>();

            if (effects != null)
            {
                heatEffect = effects.GetHeatEffect();
                coldEffect = effects.GetColdEffect();
            }
        }

        private void HandleTemperatureChangeProcess()
        {
            _waitingForCooldown = true;
            var manager = GameObject.Find("Controller").GetComponent<GameManager>();

            if (manager != null)
            {
                manager.ModifyTemperatureBy(changeValue);
                Debug.Log("Change Temperature by " + changeValue + " degrees");
            } else {
                Debug.LogError("GameManager not Found!");
            }

            if (isConsumable) 
                Destroy(gameObject);
        }
        
        private void Update()
        {
            if (_waitingForCooldown)
            {
                if (_timer < cooldownValue)
                {
                    _timer += Time.deltaTime;
                }
                else
                {
                    _timer = 0;
                    _waitingForCooldown = false;
                }
            }
        }
    }
}