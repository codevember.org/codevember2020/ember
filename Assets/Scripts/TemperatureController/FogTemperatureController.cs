﻿using UnityEngine;


// Note: Since we're using an EdgeCollider2D here, we can't use the `OnStay` function
public class FogTemperatureController : MonoBehaviour
{
    public float ChangeAmoutPerSecondInDegree = -60f;
    private int framesInFog = 0;

    private GameManager _gameManager;
    private UIManager _uiManager;

    private void Start()
    {
        _gameManager = GameManager.Instance;
        _uiManager = _gameManager.gameObject.GetComponent<UIManager>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            _gameManager.ModifyTemperatureBy(Time.deltaTime * ChangeAmoutPerSecondInDegree * (framesInFog / 30f));
            framesInFog++;
            _uiManager.EnableFogText(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            framesInFog = 0;
            _uiManager.EnableFogText(false);
        }
    }
}
