﻿using UnityEngine;

/// <summary>
///  Singleton-Pattern for MonoBehaviour
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;

	public static T Instance
	{
		get
		{
			// If the Singleton has not been initialized yet, create an empty GameObject and add the Singleton to it.
			if (!_instance)
			{
				_instance = InstantiateSingleton();
			}
			return _instance;
		}
	}

	public static bool Initialized => _instance != null;

	protected virtual void Awake()
	{
		if (!_instance && _instance != this)
		{
			_instance = (T) System.Convert.ChangeType(this, typeof(T));
		}
		else
		{
			Debug.Log(this.GetType() + " Singleton._instance already set, destroy this.");
			Destroy(gameObject);
			// _instance = (T)System.Convert.ChangeType(this, typeof(T));
		}
	}

	private static T InstantiateSingleton()
	{
        var go = new GameObject { name = typeof(T).Name };
        go.AddComponent<T>();

		return go.GetComponent<T>();
	}

	protected virtual void Start()
	{
	}


	protected virtual void OnEnable()
	{
	}
}