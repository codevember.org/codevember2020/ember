﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [Header("Linked Game Objects")] public Button playButton;

    public Button newGameButton;
    public GameObject menuElements;
    public GameObject newGameDialog;

    public void PlayGame()
    {
        if (GlobalGameSettings.GetLastUnlockedLevelNumber() == 0)
        {
            SceneManager.LoadScene("IntroScene");
        } else
        {
            SceneManager.LoadScene(GlobalGameSettings.LEVEL_LIST[Mathf.Min(GlobalGameSettings.GetLastUnlockedLevelNumber(), GlobalGameSettings.LEVEL_LIST.Count - 1)]);
        }
    }

    public void NewGameButtonPressed()
    {
        if (GlobalGameSettings.GetLastUnlockedLevelNumber() != 0)
        {
            menuElements.SetActive(false);
            newGameDialog.SetActive(true);
        } else {
            NewGameRequested();
        }
    }
    
    public void NewGameRequested()
    {
        GlobalGameSettings.PrepareForNewGame();
        PlayGame();
    }

    public void CancelNewGameDialog()
    {
        menuElements.SetActive(true);
        newGameDialog.SetActive(false);
    }

    public void QuitGame()
    {
        Debug.Log("Quitting");
        Application.Quit();
    }
}