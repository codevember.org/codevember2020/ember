﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DaylightModifier : MonoBehaviour
{
    public Gradient DaylightGradient;
    [Range(0f, 24f)]
    public float HourOfTheDay = 12f;

    public UnityEngine.Rendering.Universal.Light2D GlobalLight;
    public UnityEngine.Rendering.Universal.Light2D GlobalBackgroundLight;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateLight()
    {
        UpdateLightForDaytime(HourOfTheDay);
    }

    private void UpdateLightForDaytime(float hourOfTheDay)
    {
        var daycolor = GetColorForDaylight(hourOfTheDay);
        GlobalBackgroundLight.color = daycolor;

        Color.RGBToHSV(daycolor, out float H, out float S, out float V);
        GlobalLight.color = Color.HSVToRGB(H, S*0.5f, V);
    }

    public Color GetCurrentColorForDaylight()
    {
        return GetColorForDaylight(HourOfTheDay);
    }

    private Color GetColorForDaylight(float hourOfTheDay)
    {
        return DaylightGradient.Evaluate(hourOfTheDay / 24f);
    }

    // Note: Check [ExecuteInEditMode]
    public void SimulateADay(float seconds)
    {
        StartCoroutine(SimulateADayCoroutine(2f));
    }
    
    public IEnumerator SimulateADayCoroutine(float seconds)
    {
        for (int i = 0; i < 24 * seconds; i++)
        {
            Debug.Log(i / 24f / seconds);
            UpdateLightForDaytime(i/seconds);
            yield return new WaitForSeconds(1f/24f);
        }
    }
}
