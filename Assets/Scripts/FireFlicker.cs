﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FireFlicker : MonoBehaviour
{
    private float _initialIntensity;
    private UnityEngine.Rendering.Universal.Light2D _light2D;

    [Range(0,4)]
    public float FlickeringAmplitude = 1f;
    [Range(0.1f, 5f)]
    public float FlickeringSpeed = 2f;

    private void Start()
    {
        _light2D = GetComponent<UnityEngine.Rendering.Universal.Light2D>();
        _initialIntensity = _light2D.intensity;
    }

    private void Update()
    {
        _light2D.intensity = _initialIntensity + (Mathf.PerlinNoise(Time.timeSinceLevelLoad * FlickeringSpeed, 1f) - 0.5f) * 2f * FlickeringAmplitude;
    }
}
