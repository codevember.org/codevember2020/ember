﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalGameSettings
{
    #region Difficulty Modifier

    private const int DIFFICULTY_MODIFIER_EASY = 50;
    private const int DIFFICULTY_MODIFIER_HARD = 100;
    public static int initialDifficultyModifier = DIFFICULTY_MODIFIER_EASY;

    #endregion

    #region LevelSettings

    public static readonly List<string> LEVEL_LIST = new List<string>
    {
        // Intro cutscene, then on top of the mountain

        "Level_00", // Intro
        "Level_00_a", // One fire jump
        "Level_00_b", // Two fire jumps
        "Level_01", // some practice...
        "Level_02", // and a bit more...

        // Transition scene

        "Level_03", // Start in "the real world"
        "Level_04", // Intro one stroke
        "Level_05", // some practice
        "Level_06", // some more practice
        "Level_07", // falling/low power stroke
        "Level_08", // climbing
        "Level_09", // one stroke finale: reach the top

        "Level_10", // *** Large shot intro
        "Level_11", // train the correction shots
        "Level_12", // fly around something
        "Level_13", // drop altitude and braking
        "Level_14", // combined and fog
        "Level_15", // small path through the fog

        "Level_20", // *** temperature controller (rune) intro
        "Level_21", // intro ending
        "Level_22", // temperature minus, alternative path
        "Level_23", // fog is clearing
        "Level_24", // fog in the cave!!1 - credits by Julian
        "Level_25", // go far or go home

        "Level_30", // *** climbing the mountain I
        "Level_31", // jump left and right, introduce the restricted slo mo timer: 5s
        "Level_32", // restrict to 4s
        "Level_33", // further climbing, restrict to 2s
        "Level_34", // last tent and settler (first finale): further climbing, restrict to 1s

        //"Level_40", // old stuff
        //"Level_50",
    };

    private const string LAST_PLAYED_LEVEL_STRING = "LastPlayedLevel";
    private const string LAST_PLAYED_LEVEL_ITERATION_STRING = "LastPlayedLevelIteration";
    private const string LAST_UNLOCKED_LEVEL_STRING = "LastUnlockedLevel";

    // Iteration related block
    public static int GetLastPlayedLevelIteration()
    {
        return PlayerPrefs.GetInt(LAST_PLAYED_LEVEL_ITERATION_STRING, 0);
    }

    public static void SetLastPlayedLevelIteration(int iteration)
    {
        PlayerPrefs.SetInt(LAST_PLAYED_LEVEL_ITERATION_STRING, iteration);
    }

    public static void ResetLastPlayedLevelIteration()
    {
        PlayerPrefs.SetInt(LAST_PLAYED_LEVEL_ITERATION_STRING, 0);
    }

    public static void IncreaseLastPlayedLevelIteration()
    {
        var currentLevelIteration = GetLastPlayedLevelIteration() + 1;

        // Update fluid difficulty: If a user gets stuck, make the game easier
        Debug.Log("Current level iteration: " + currentLevelIteration);
        if (currentLevelIteration % 5 == 0)
        {
            switch (currentLevelIteration)
            {
                case 5:
                    initialDifficultyModifier += 5;
                    break;
                case 10:
                    initialDifficultyModifier += 5;
                    break;
                case 25:
                    initialDifficultyModifier += 5;
                    break;
                case 50:
                    initialDifficultyModifier += 5;
                    break;
                case 100:
                    initialDifficultyModifier += 10;
                    break;
            }
        }

        PlayerPrefs.SetInt(LAST_PLAYED_LEVEL_ITERATION_STRING, currentLevelIteration);
    }
    // End iteration related block

    public static void SaveLastPlayedLevelNumber(int levelNumber)
    {
        PlayerPrefs.SetInt(LAST_PLAYED_LEVEL_STRING, levelNumber);
    }

    public static void UpdateLastUnlockedLevelNumber(int levelNumber)
    {
        // Only update the last unlocked level with the highest number
        var lastUnlockedLevel = PlayerPrefs.GetInt(LAST_UNLOCKED_LEVEL_STRING);
        PlayerPrefs.SetInt(LAST_UNLOCKED_LEVEL_STRING, Math.Max(lastUnlockedLevel, levelNumber + 1));
    }

    public static void ResetLastUnlockedLevelNumber()
    {
        PlayerPrefs.SetInt(LAST_UNLOCKED_LEVEL_STRING, 0);
    }

    public static int GetLastPlayedLevelNumber()
    {
        return PlayerPrefs.GetInt(LAST_PLAYED_LEVEL_STRING);
    }

    public static int GetLastUnlockedLevelNumber()
    {
        var levelNumber = PlayerPrefs.GetInt(LAST_UNLOCKED_LEVEL_STRING);
        return levelNumber;
    }

    public static void PrepareForNewGame()
    {
        // Reset latest unlocked level
        SaveLastPlayedLevelNumber(0);
        ResetLastUnlockedLevelNumber();
        ResetLastPlayedLevelIteration();
        // Reset scores and counter
        ResetCaughtCount();
        // Note: You don't want to reset the achievements :)
    }

    #endregion

    #region AudioSettings

    public const float MAX_AUDIO_VOLUME = 0.6f;
    private const string AUDIO_MUTED_STRING = "AudioMuted";

    public static bool ToggleMuteAudio()
    {
        var newState = !AudioMuted();
        PlayerPrefs.SetInt(AUDIO_MUTED_STRING, newState ? 1 : 0);
        return newState;
    }

    public static bool AudioMuted()
    {
        return PlayerPrefs.GetInt(AUDIO_MUTED_STRING) == 1;
    }

    #endregion

    #region QualitySettings

    public enum Quality : int
    {
        Medium = 0,
        Low = 1,
        High = 2
    }

    private static Quality GetQualityForInt(int value)
    {
        if (Enum.IsDefined(typeof(Quality), value))
        {
            return (Quality) value;
        }
        else
        {
            Debug.LogError("GlobalGameSettings#GetQualityForInt: Invalid value " + value);
            return Quality.Medium; // Fallback
        }
    }

    private const string GRAPHICS_QUALITY_STRING = "Quality";

    private const int PIXEL_LIGHT_COUNT_LOW = 5;
    private const int PIXEL_LIGHT_COUNT_MEDIUM = 20;
    private const int PIXEL_LIGHT_COUNT_HIGH = 28;

    private const int SHOW_DISTANCE_LOW = 15;
    private const int SHOW_DISTANCE_MEDIUM = 20;
    private const int SHOW_DISTANCE_HIGH = 22;

    private const ShadowQuality SHADOW_QUALITY_LOW = ShadowQuality.HardOnly;
    private const ShadowQuality SHADOW_QUALITY_MEDIUM = ShadowQuality.All;
    private const ShadowQuality SHADOW_QUALITY_HIGH = ShadowQuality.All;

    private const ShadowProjection SHADOW_PROJECTION_LOW = ShadowProjection.CloseFit;
    private const ShadowProjection SHADOW_PROJECTION_MEDIUM = ShadowProjection.StableFit;
    private const ShadowProjection SHADOW_PROJECTION_HIGH = ShadowProjection.StableFit;

    private const ShadowResolution SHADOW_RESOLUTION_LOW = ShadowResolution.Low;
    private const ShadowResolution SHADOW_RESOLUTION_MEDIUM = ShadowResolution.Medium;
    private const ShadowResolution SHADOW_RESOLUTION_HIGH = ShadowResolution.High;

    private const int ANTI_ALIASING_LOW = 0;
    private const int ANTI_ALIASING_MEDIUM = 0;
    private const int ANTI_ALIASING_HIGH = 2;

    public static Quality ToggleGraphicsQuality()
    {
        var newQuality = Quality.Medium;
        switch (GetCurrentQuality())
        {
            case Quality.Low:
                newQuality = Quality.Medium;
                break;
            case Quality.Medium:
                newQuality = Quality.High;
                break;
            case Quality.High:
                newQuality = Quality.Low;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        PlayerPrefs.SetInt(GRAPHICS_QUALITY_STRING, (int) newQuality);
        ApplyGraphicsQuality();

        return newQuality;
    }

    public static void ApplyGraphicsQuality()
    {
        // ## Set quality settings ##
        var currentQuality = GetCurrentQuality();
        switch (currentQuality)
        {
            case Quality.Low:
            {
                QualitySettings.pixelLightCount = PIXEL_LIGHT_COUNT_LOW;
                QualitySettings.shadowDistance = SHOW_DISTANCE_LOW;
                QualitySettings.shadows = SHADOW_QUALITY_LOW;
                QualitySettings.shadowProjection = SHADOW_PROJECTION_LOW;
                QualitySettings.shadowResolution = SHADOW_RESOLUTION_LOW;
                QualitySettings.antiAliasing = ANTI_ALIASING_LOW;
                break;
            }
            case Quality.Medium:
            {
                QualitySettings.pixelLightCount = PIXEL_LIGHT_COUNT_MEDIUM;
                QualitySettings.shadowDistance = SHOW_DISTANCE_MEDIUM;
                QualitySettings.shadows = SHADOW_QUALITY_MEDIUM;
                QualitySettings.shadowProjection = SHADOW_PROJECTION_MEDIUM;
                QualitySettings.shadowResolution = SHADOW_RESOLUTION_MEDIUM;
                QualitySettings.antiAliasing = ANTI_ALIASING_MEDIUM;
                break;
            }
            case Quality.High:
            {
                QualitySettings.pixelLightCount = PIXEL_LIGHT_COUNT_HIGH;
                QualitySettings.shadowDistance = SHOW_DISTANCE_HIGH;
                QualitySettings.shadows = SHADOW_QUALITY_HIGH;
                QualitySettings.shadowProjection = SHADOW_PROJECTION_HIGH;
                QualitySettings.shadowResolution = SHADOW_RESOLUTION_HIGH;
                QualitySettings.antiAliasing = ANTI_ALIASING_HIGH;
                break;
            }
            default:
                throw new ArgumentOutOfRangeException();
        }

        Debug.Log(">>> Set quality to " + currentQuality);
        Debug.Log("Set pixelLightCount to " + QualitySettings.pixelLightCount);
        Debug.Log("Set shadowDistance to " + QualitySettings.shadowDistance);
        Debug.Log("Set shadows to " + QualitySettings.shadows);
        Debug.Log("Set shadowProjection to " + QualitySettings.shadowProjection);
        Debug.Log("Set shadowResolution to " + QualitySettings.shadowResolution);
        Debug.Log("Set antiAliasing to " + QualitySettings.antiAliasing);
    }

    public static Quality GetCurrentQuality()
    {
        return GetQualityForInt(PlayerPrefs.GetInt(GRAPHICS_QUALITY_STRING));
    }

    public static string GetCurrentQualityString()
    {
        switch (GetCurrentQuality())
        {
            case Quality.Low: return "Quality: Low";
            case Quality.Medium: return "Quality: Medium";
            case Quality.High: return "Quality: High";
            default:
                return "Unknown";
        }
    }

    #endregion

    #region Scores and Acchievements

    private const string DEATH_COUNT_STRING = "DeathCount";

    // Iteration related block
    public static int GetDeathCount()
    {
        return PlayerPrefs.GetInt(DEATH_COUNT_STRING, 0);
    }

    public static void IncreaseDeathCount()
    {
        PlayerPrefs.SetInt(DEATH_COUNT_STRING, GetDeathCount() + 1);
    }

    public static void ResetCaughtCount()
    {
        PlayerPrefs.SetInt(DEATH_COUNT_STRING, 0);
    }

    #endregion

    #region Misc Game Settings

    private const string CHANGELOG_SEEN_STRING = "ChangelogSeen";
    private const string NOT_FIRST_TIME_PLAYING_STRING = "NotFirstTimePlaying";


    public static bool ChangelogSeen
    {
        get => PlayerPrefs.GetInt(CHANGELOG_SEEN_STRING) == 1;
        set => PlayerPrefs.SetInt(CHANGELOG_SEEN_STRING, value ? 1 : 0);
    }

    public static bool NotFirstTimePlaying
    {
        get => PlayerPrefs.GetInt(NOT_FIRST_TIME_PLAYING_STRING) == 1;
        set => PlayerPrefs.SetInt(NOT_FIRST_TIME_PLAYING_STRING, value ? 1 : 0);
    }

    #endregion

    #region PostProcessing Values

    public const float BLOOM_INTENSITY = 12f;
    public const float CC_SATURATION = 50f;
    public const float CC_CONTRAST = 10f;
    public const float LENS_DISTORTION_INTENSITY = -40f;
    public const float VIGNETTE_INTENSITY = 0.36f;

    public static GameObject levelGameObject = null;

    #endregion
}