﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameFromIntro : MonoBehaviour
{
    public bool isEnd = false;

    void Start()
    {
        if (!isEnd) StartCoroutine(StartGame());
        else StartCoroutine(ReturnToMenu());
    }

    private IEnumerator StartGame()
    {
        Debug.Log("StartGameFromIntro...");
        yield return new WaitForSecondsRealtime(5);
        SceneManager.LoadScene(GlobalGameSettings.LEVEL_LIST[0]);
    }

    private IEnumerator ReturnToMenu()
    {
        Debug.Log("StartGameFromIntro... Returning to menu");
        yield return new WaitForSecondsRealtime(10);
        SceneManager.LoadScene("MenuScene");
    }
}
