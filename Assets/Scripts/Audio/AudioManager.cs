﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

// Lofi song from: https://www.youtube.com/watch?v=eqln0CiGs4I
public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    [FormerlySerializedAs("audioMuted")] 
    public bool isAudioMuted;

    [FormerlySerializedAs("currentlyPlayingMusic")] [FormerlySerializedAs("currentPlayingMusic")]
    [FormerlySerializedAs("currentlyPlayingSound")]
    public Sound currentlyPlayedSong;
    public Sound[] sounds;

    private static readonly string INITAL_SONG_TITLE = "Theme";

    private const int BUTTON_SOUND_INDEX = 8;

    private void Awake()
    {
        DontDestroyOnLoad(this);

        if(instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;

        foreach (var sound in sounds)
        {
            sound.MapAudioSource(gameObject.AddComponent<AudioSource>());
        }

        if (currentlyPlayedSong.clip == null)
        {
            currentlyPlayedSong = sounds[0];
            Debug.Log("No Song was set, therefore the first availble song is set.");
        }
    }

    private void Start()
    {
        Play(INITAL_SONG_TITLE);
    }

    public void Play(string soundName)
    {
        //Debug.Log("playing sound: " + soundName);
    
        var sound = FindSound(soundName);
        if (sound == null)
        {
            Debug.LogWarning("Sound: " + soundName + " not found!");
            return;
        }

        if (sound.type == Sound.SoundType.Music)
        {
            //Debug.Log("Note: This sound is of type music");
            currentlyPlayedSong = sound;
        }
            
        if (!isAudioMuted)
        {
            sound.audioSource.Play();
        }  
    }

    private void Stop(string soundName)
    {
        var sound = FindSound(soundName);
        if (sound == null)
        {
            Debug.LogWarning("Sound: " + soundName + " not found!");
            return;
        }

        sound.audioSource.Stop();
    }

    public void StopAllSounds()
    {
        foreach (var sound in sounds)
            if (sound.type == Sound.SoundType.Sound)
                Stop(sound.name);
        Debug.Log("Sounds Stopped");
    }

    public void Pause(string soundName)
    {
        var sound = FindSound(soundName);
        if (sound == null)
        {
            Debug.LogWarning("Sound: " + soundName + " not found!");
            return;
        }

        sound.audioSource.Pause();
    }

    public void UnPause(string soundName)
    {
        var sound = FindSound(soundName);
        if (sound == null)
        {
            Debug.LogWarning("Sound: " + soundName + " not found!");
            return;
        }

        sound.audioSource.UnPause();
    }

    public void PauseSounds()
    {
        foreach (var sound in sounds)
            if (sound.type == Sound.SoundType.Sound)
                sound.audioSource.Pause();

        Debug.Log("Sounds paused");
    }

    public void UnPauseSounds()
    {
        foreach (var sound in sounds)
            if (sound.type == Sound.SoundType.Sound)
                sound.audioSource.UnPause();

        Debug.Log("Sounds unpaused");
    }

    public void PlayButtonSound()
    {
        if (!isAudioMuted)
            Play(sounds[BUTTON_SOUND_INDEX].name);
    }

    public void Fade(string soundName, float duration, float targetVolume)
    {
        StartCoroutine(StartFade(soundName, duration, targetVolume));
    }

    private IEnumerator StartFade(string soundName, float duration, float targetVolume)
    {
        var sound = FindSound(soundName);
        if (sound == null)
        {
            Debug.LogWarning("Sound: " + soundName + " not found!");
            yield return new WaitForEndOfFrame();
        }

        //TODO resolve possible NPE of sound
        var audioSource = sound.audioSource;
        float currentTime = 0;
        var volume = audioSource.volume;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(volume, targetVolume, currentTime / duration);
            yield return null;
        }

        if (targetVolume == 0) 
            sound.audioSource.Stop();
    }

    private Sound FindSound(string soundName)
    {
        return Array.Find(sounds, s => s.name == soundName);
    }
}