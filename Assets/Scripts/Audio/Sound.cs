﻿using UnityEngine;

[System.Serializable]
public class Sound 
{
    public enum SoundType
    {
        Music,
        Sound
    }
    
    public string name;
    public SoundType type;

    public AudioClip clip;

    [Range(0f, 1f)]
    public float volume;
    [Range(0.1f, 3f)]
    public float pitch;

    public bool loop;

    [HideInInspector]
    public AudioSource audioSource;

    public void MapAudioSource(AudioSource audioSource)
    {
        audioSource.clip = this.clip;
        audioSource.volume = this.volume;
        audioSource.pitch = this.pitch;
        audioSource.loop = this.loop;
        this.audioSource = audioSource;
    }
}
