﻿using UnityEngine;

public class Destroy : MonoBehaviour
{
    private float _targetTime;
    private float _passedTime;
  
    void Update()
    {
        if (_passedTime > _targetTime)
        {
            Destroy(gameObject);
        }

        _passedTime += Time.deltaTime;
    }

    public void DestroyOverTime(float seconds)
    {
        this._targetTime = seconds;
    }
}
