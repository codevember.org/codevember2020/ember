﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxEffect : MonoBehaviour
{
    private float length, startPosition, yOffset;

    private GameObject _player;
    private Camera _camera;

    public bool alsoFollowInYDirection = false;

    [Range(0,1)]
    [Tooltip("1 is basically: moving with the camera and <1 is partly movement")]
    public float ParallaxEffectValue;

    private void Awake()
    {
        if (transform.name.Contains("Clone")) Destroy(GetComponent<ParallaxEffect>());
    }

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
        _player = GameManager.Instance.Player;
        _camera = GameManager.Instance.currentCamera;
        yOffset = transform.position.y - _player.transform.position.y;

        // If this is the parent element, create two children - else drop the parallax component
        if (!transform.name.Contains("Clone"))
        {
            var ImageLeft = Instantiate(this.gameObject, transform);
            ImageLeft.transform.position = new Vector3(ImageLeft.transform.position.x - length, 0, ImageLeft.transform.position.z);
            ImageLeft.transform.localPosition = new Vector3(ImageLeft.transform.localPosition.x, 0, ImageLeft.transform.localPosition.z);
            ImageLeft.name += "_leftClone";

            var ImageRight = Instantiate(this.gameObject, transform);
            // Make sure not child is copied
            foreach (Transform child in ImageRight.transform)
            {
                if (child.transform.name != "Fill") Destroy(child.gameObject);
            }
            ImageRight.transform.position = new Vector3(ImageRight.transform.position.x + length, 0, ImageRight.transform.position.z);
            ImageRight.transform.localPosition = new Vector3(ImageRight.transform.localPosition.x, 0, ImageRight.transform.localPosition.z);
            ImageRight.name += "_rightClone";
        }
    }

    private void LateUpdate()
    {
        float dist = _camera.transform.position.x * ParallaxEffectValue;

        var yValue = (alsoFollowInYDirection) ? _camera.transform.position.y + yOffset : transform.position.y;
        var targetPosition = new Vector3(startPosition + dist, yValue, transform.position.z);
        transform.position = targetPosition;

        // Jump one step to the left or one to the right, if needed
        float tempPosition = _camera.transform.position.x * (1 - ParallaxEffectValue);
        if (tempPosition > startPosition + length) startPosition += length;
        else if (tempPosition < startPosition - length) startPosition -= length;
    }
}
