﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using UnityEditor;

/*
 * Ignite objects 
 */
public class Tinder : MonoBehaviour
{
    [Header("Parameter")]
    public float burnThreshold = float.MaxValue;
    public float tempIncreaseStep;
    [FormerlySerializedAs("GameObjectToDisableOnIgnition")] [Header("Linked Game Objects")]
    public GameObject gameObjectToDisableOnIgnition;
    public GameObject gameObjectToEnableOnIgnition;
    [Space]
    public GameObject fireEffect;
    public Canvas tinderCanvas;
    public Slider slider;
    public bool allowReEntering = true;
    public bool resetStrokeCount = true;

    [SerializeField]
    private float currentTemp;
    private bool _isIgniting;
    private float _cachedPlayerTemp;

    private Rigidbody2D _playerRigidbody;
    private CharacterController _characterController;
    private GameManager _gameManager;
    private float _initialTemperature;
    private GameObject _targetParticles;

    [Header("Debug")]
    public bool startHereWithPlayer;

    private void Start()
    {
        _gameManager = GameManager.Instance;
        tinderCanvas.worldCamera = _gameManager.currentCamera;
        _characterController = _gameManager.Player.GetComponent<CharacterController>();
        _initialTemperature = currentTemp;
        tinderCanvas.enabled = false;

        var targetParticles = gameObject.transform.Find("TargetParticles");
        if (targetParticles)
        {
            _targetParticles = targetParticles.gameObject;
            activateTargetParticles(false);
        }

        // Set the player to this fire as a starting position
        // ... also make sure that is isn't set in production
        if (startHereWithPlayer && Debug.isDebugBuild)
        {
            _gameManager.Player.transform.position = transform.position + Vector3.up;
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            // Do nothing if already ignited and reentering is not enabled
            if (fireEffect.activeSelf && !allowReEntering) 
                return;
            // ... if it should reset the stroke count, allow this as well
            if (fireEffect.activeSelf && resetStrokeCount)
                _gameManager.ResetCurrentStrokeCount();

            if (!_playerRigidbody)
                _playerRigidbody = other.gameObject.GetComponent<Rigidbody2D>();

            // Clamp more if player is falling down
            var maxLength = (_playerRigidbody.velocity.y < 0) ? 0.1f : 0.2f;
            _playerRigidbody.velocity = Vector3.ClampMagnitude(_playerRigidbody.velocity, maxLength);            

            _cachedPlayerTemp = _gameManager.currentTemp;
            if (_cachedPlayerTemp > burnThreshold)
            {
                _isIgniting = true;
                GameManager.Instance.isCurrentlyIgniting = _isIgniting;
                _playerRigidbody.gravityScale = _gameManager.invertedPlayerGravityScale;
            }  
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            StopIgnition();
        }
    }

#if UNITY_EDITOR
    // For easier leveldesign: show the current GO name above the fireplace
    private void OnDrawGizmos()
    {
        Handles.Label(transform.position + Vector3.up, this.name);
    }
#endif

    private void StopIgnition()
    {
        if (!_isIgniting) 
            return;

        // Stopping ignition
        _isIgniting = false;
        _gameManager.isCurrentlyIgniting = _isIgniting;
        _playerRigidbody.gravityScale = _gameManager.normalPlayerGravityScale;
    }

    public bool IsBurning()
    {
        return currentTemp > burnThreshold;
    }
    
    private void UpdateTinderSliderValue()
    {
        if (slider != null && !fireEffect.activeSelf)
        {
            tinderCanvas.enabled = true;
            slider.value = (currentTemp - _initialTemperature) / (burnThreshold - _initialTemperature);
            //tinderText.text = Mathf.RoundToInt(currentTemp).ToString() + "°C" + " / \n" + burnThreshold.ToString() + "°C";
        }
        else
        {
            tinderCanvas.enabled = false;
        }
    }

    private void FixedUpdate()
    {
        if (GameManager.IsPlayerDead())
        {
            StopIgnition();
            return;
        }

        var deltaTime= Time.deltaTime;
        if(_isIgniting)
        {
            currentTemp += (_cachedPlayerTemp / 10f) * deltaTime;
            UpdateTinderSliderValue();
            
            // If the fireplace is actually ingited:
            if (currentTemp > burnThreshold && !fireEffect.activeSelf)
            {
                _gameManager.ModifyTemperatureBy(tempIncreaseStep);
                fireEffect.SetActive(true);
                _gameManager.ResetCurrentStrokeCount();
                if (gameObjectToDisableOnIgnition)
                    gameObjectToDisableOnIgnition.SetActive(false);
                if (gameObjectToEnableOnIgnition)
                    gameObjectToEnableOnIgnition.SetActive(true);
                AudioManager.instance.Play("Ignition");
                // Deactivate the target particles an light for a performance improvement
                activateTargetParticles(false);
                //Handheld.Vibrate(); Note: This seems to be blocking on android devices and takes around 200 - 400ms 
            }        
        }
    }

    public void activateTargetParticles(bool active)
    {
        if (_targetParticles) _targetParticles.SetActive(active);
    }

}
