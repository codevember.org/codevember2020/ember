﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using static UnityEngine.ParticleSystem;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [Header("Game State")]
    public float currentTemp = 225f;

    [Header("Character controller Parameter")]
    [Tooltip("Slo mo factor if player is currently dragging the cursor: 1 is normal speed")]
    [Range(0.01f, 1f)]
    public float touchSlowMoFactor = 1.0f;
    [Tooltip("The main stroke has always a multiplier of 1, " +
        "change the power in the power in the characterController")]
    private int numberOfMainStrokes = 1; // Note: A change of this variable is not yet planned and implemented
    public int numberOfCorrectionStrokes = 3;
    [FormerlySerializedAs("correctionStrokeMuliplier")] [Range(0.01f, 1)]
    public float correctionStrokeMultiplier = 0.1f;
    private int _currentStroke;

    [Tooltip("Applies force to the player when he/she's flying into the direction of the next target")]
    public float AutoCorrectionMultiplier = 0.1f;
    private float _initialAutoCorrectionMulitplier;

    [Space]
    public float windForcePower = 20f;
    public int windForceApplicationSteps = 10;
    public float clampValueForMagnitude = 5f;
    public float particleForceFieldMultiplier = 3f;

    [Header("Gravity Parameter")]
    public float normalPlayerGravityScale = 0.1f;
    [Tooltip("This is used, when you're igniting a prop")]
    public float invertedPlayerGravityScale = -0.01f;

    [Header("Camera Control Parameter")]
    // How many units should we keep from the players
    public float CameraControlZoomMinimum = 3;
    public float CameraControlZoomPadding = 0.5f;

    private Transform latestClosestTarget = null;
    private bool cameraPositionSmoothingActive = false;

    [Header("Settings Parameter")]
    public float metersPerUnit = 3f;
    [Tooltip("If !=0, this floating number defines the time of the maximal available slo motion time while touching")]
    public float maxSloMoTime = 0f;

    [Header("Linked Game Objects")]
    public Camera currentCamera;
    public ParticleSystem ambientParticles;
    public GameObject Player;
    public GameObject PlayerParticles;
    public ColorOverLifetimeModule _colorOverLifetimeModule;
    public MinMaxGradient _playerParticleGradient;
    public LineRenderer nextTargetLine;
    public LineRenderer nextTargetDot;
    public Animator transition;
    public ParticleSystem heatEffect;
    public ParticleSystem coldEffect;
    public GameObject TargetsContainer;
    public GameObject TransitionText;
    public GameObject MainStrokeIcon;

    private ShapeModule _ambientShapeModule;
    private UIManager _uiManager;
    private SpriteRenderer _playerSprite;
    private Rigidbody2D _playerRigidbody;
    private static bool _isRestarted = true;

    [HideInInspector]
    public bool isCurrentlyIgniting = false; // Could be a getter/setter

    [SerializeField] public const int TARGET_TEMPERATURE = 1200;
    [SerializeField] public const int WARNING_TEMPERATURE = 220;
    [SerializeField] public const int LOWEST_TEMPERATURE = 180;

    //  Current game state
    private static bool _goalAchieved;
    private static bool _isPlayerDead;

    // Animations
    private int _lastCachedLevel = -1;
    public Animator playerAnimator; // TODO: Extract animation methods
    private const string ANIMATION_STATE_LEVEL_TAG = "FlameState";
    private const int EFFECT_THRESHOLD = 5;

    // Mixed internal vars
    private bool _transitioning = false;
    private readonly Color _playerColorLow = new Color(0.5f, 0.5f, 0.5f, 1.0f);
    private TemperatureMassDefinition[] _tempSideEffectsValues; // Level settings
    private int _currentLevelIndex = 0;
    private float lastCamZoom = 0f;
    private Coroutine smoothingCoroutine;
    private int levelToLoad = -1;

    // Configuration parameters for the 
    private readonly float maxCamZoomForFocussingBothTransforms = 10f;
    private readonly float maxClampValue = 20f;
    private readonly float minimumShiftingToMidpoint = 0.1f; //[Range(0,1f)]
    private bool lastUsedHorizontalCamLayout = true;

    // Debug
    GameObject _cachedFPSCounter;

    private void Awake()
    {
        // Set the current instance
        Instance = this;
    }
    
    private void Start()
    {
        Debug.Log("GameManager::Start");
        _ambientShapeModule = ambientParticles.shape;
        _uiManager = GetComponent<UIManager>();
        _playerRigidbody = Player.GetComponent<Rigidbody2D>();
        _colorOverLifetimeModule = PlayerParticles.GetComponent<ParticleSystem>().colorOverLifetime;
        _playerParticleGradient = _colorOverLifetimeModule.color;
        _playerSprite = Player.GetComponent<SpriteRenderer>();
        _isPlayerDead = false;
        _goalAchieved = false;
        Time.timeScale = 1f;
        _tempSideEffectsValues = new [] {
            new TemperatureMassDefinition(225f, 0.1f, 0, new Color(0.5f, 0.0f, 0.0f, 0.9f)), // TODO Adjust colors
            new TemperatureMassDefinition(400f, 0.1f, 0, new Color(1.0f, 0.0f, 0.0f, 0.9f)),
            new TemperatureMassDefinition(600f, 0.09f, 1, new Color(1.0f, 0.1f, 0.0f, 0.9f)),
            new TemperatureMassDefinition(1000f, 0.08f, 2, new Color(1.0f, 0.4f, 0.0f, 0.9f))
        };
        Debug.Log("Looking up level for scene name: " + SceneManager.GetActiveScene().name);
        _currentLevelIndex = CurrentLevelIndex();
        Debug.Log("Parsed level no.:" + _currentLevelIndex);
        //_uiManager.SetLevelStartText(_currentLevelIndex);

        // Persist this state for the transition
        if (_isRestarted)
        {
            _uiManager.ClearLevelStartText();
            _isRestarted = false;
        }
        _uiManager.UpdateMinorStrokeCount(numberOfCorrectionStrokes);
        _initialAutoCorrectionMulitplier = AutoCorrectionMultiplier;
    }

    public static int CurrentLevelIndex()
    {
        return GlobalGameSettings.LEVEL_LIST.IndexOf(SceneManager.GetActiveScene().name);
    }

    private bool isObjectVisibleForCamera(Camera camera, Transform transform)
    {
        Vector3 viewPos = camera.WorldToViewportPoint(transform.position);
        return viewPos.x >= 0 && viewPos.x <= 1 && viewPos.y >= 0 && viewPos.y <= 1 && viewPos.z > 0;
    }

    // Note: Always execute camera movement following physic objects in the LateUpdate
    private void LateUpdate()
    {
        var elementsToIgnite = 0;
        var ignitedElements = 0;
        var closestDistance = float.MaxValue;
        var playerPosition = Player.transform.position;
        Transform closestTarget = null;
        foreach (Transform child in TargetsContainer.transform)
        {
            if (!child.gameObject.activeSelf) continue;

            if (child.GetComponent<Tinder>().IsBurning()) {
                ignitedElements++;
            } else {
                elementsToIgnite++;

                var actualDistance = Vector3.Distance(playerPosition, child.position);
                if (actualDistance < closestDistance)
                {
                    closestDistance = actualDistance;
                    closestTarget = child;
                }
            }
        }
        
        // Draw the next target line
        if (!_isPlayerDead && closestTarget != null)
        {
            FixedCameraFollowSmooth(currentCamera, Player.transform, closestTarget.transform);

            var startingPosition = playerPosition;
            startingPosition.z = 1f;
            nextTargetLine.SetPosition(0, startingPosition);
            nextTargetLine.SetPosition(1, closestTarget.position);
            nextTargetLine.enabled = true;

            //var nextTargetPosition = startingPosition + ;
            //nextTargetPosition.y *= 2f;

            if (isObjectVisibleForCamera(currentCamera, closestTarget))
            {
                nextTargetDot.enabled = false;
            } else
            {
                var direction = Vector3.Normalize(closestTarget.position - currentCamera.transform.position);
                direction.z = 0;
                var nextTargetPosition = currentCamera.ScreenToWorldPoint(new Vector3(Screen.width / 2 * (1 + direction.x), Screen.height / 2 * (1 + direction.y), currentCamera.nearClipPlane));
                nextTargetDot.SetPosition(0, nextTargetPosition - (nextTargetPosition - playerPosition)*0.05f);
                nextTargetDot.SetPosition(1, nextTargetPosition);
                nextTargetDot.enabled = true;
            }
            
            // Lead the player to the next target 
            if (_playerRigidbody.gravityScale == normalPlayerGravityScale && _currentStroke != 0)
            {
                _playerRigidbody.AddForce((closestTarget.position - playerPosition).normalized * AutoCorrectionMultiplier);
            }

            if (closestTarget != latestClosestTarget)
            {
                //Debug.Log("Closest target changed");
                InitSmoothingCoroutine();
                if (latestClosestTarget) latestClosestTarget.GetComponent<Tinder>().activateTargetParticles(false);
                closestTarget.GetComponent<Tinder>().activateTargetParticles(true);
            }
            latestClosestTarget = closestTarget;
        }

        // Check for the game mechanics
        // Note: Different goal (e.g. amount of ignited objects / currentTemp > TARGET_TEMPERATURE)
        if (elementsToIgnite == 0 && !_goalAchieved)
        {
            Debug.Log("Goal achieved: all elements ignited!");
            LevelGoalAchieved();
        }
        else if (currentTemp <= LOWEST_TEMPERATURE && !_goalAchieved && !_isPlayerDead)
        {
            Debug.Log("Player temperature too low. Player is dying.");
            PlayerDied();
        }

        _ambientShapeModule.position = playerPosition;
        
        // Code for easier debugging/testing
        if (Input.GetKeyDown(KeyCode.Space) && Debug.isDebugBuild)
        {
            AutoCorrectionMultiplier = 5f;
        } else if (Input.GetKeyUp(KeyCode.Space) && Debug.isDebugBuild)
        {
            AutoCorrectionMultiplier = _initialAutoCorrectionMulitplier;
        }
    }

    //TODO move to a better place within project
    private void FixedCameraFollowSmooth(Camera cam, Transform t1, Transform t2)
    {
        // Midpoint we're after
        Vector3 midpoint = (t1.position + t2.position) / 2f;
        var twoToOne = (t1.position - t2.position);

        float camZoom;
        if (Mathf.Abs(twoToOne.x / twoToOne.y) < (float)Screen.width / (float)Screen.height)
        {
            // Fit in Vertically
            camZoom = Mathf.Abs(twoToOne.y) + CameraControlZoomPadding / 2;
            if (lastUsedHorizontalCamLayout)
            {
                InitSmoothingCoroutine();
            }
            lastUsedHorizontalCamLayout = false;
        }
        else
        {
            // Fit in Horizontally
            camZoom = Mathf.Max(CameraControlZoomMinimum, Mathf.Abs(twoToOne.x) * Screen.height / Screen.width * 0.5f + CameraControlZoomPadding);
            if (!lastUsedHorizontalCamLayout)
            {
                InitSmoothingCoroutine();
            }
            lastUsedHorizontalCamLayout = true;
        }

        cam.orthographicSize = Mathf.Max(CameraControlZoomMinimum, Mathf.Lerp(cam.orthographicSize, camZoom, 0.05f));

        if (camZoom > maxCamZoomForFocussingBothTransforms)
        {
            var t1ToMidpoint = t1.position - midpoint;
            var clampedZoom = Mathf.Clamp(camZoom, maxCamZoomForFocussingBothTransforms, maxClampValue);
            // at 10-15: midpoint = t1.position - twoToOne / 4f * (1f- (Mathf.Abs(clampedZoom - 12.5f) / 2.5f));
            midpoint = t1.position - t1ToMidpoint * Math.Max(minimumShiftingToMidpoint, (1f - (Mathf.Abs(clampedZoom - maxCamZoomForFocussingBothTransforms) / (maxClampValue - maxCamZoomForFocussingBothTransforms))));
            cam.orthographicSize = 10f;
        }

        // Make sure to visualize smooth transitions
        if (camZoom > 10f && lastCamZoom <= 10 || camZoom < 10f && lastCamZoom >= 10)
        {
            InitSmoothingCoroutine();
        }

        lastCamZoom = camZoom;

        // Make sure to don't overwrite the z coordinate
        midpoint.z = cam.transform.position.z;

        // Only lerp the camera position, if the closest target had changed recently
        if (cameraPositionSmoothingActive)
        {
            cam.transform.position = Vector3.Lerp(cam.transform.position, midpoint, 0.2f);
        }
        else
        {
            cam.transform.position = midpoint;
        }
    }

    private void InitSmoothingCoroutine()
    {
        if (smoothingCoroutine != null) StopCoroutine(smoothingCoroutine);
        smoothingCoroutine = StartCoroutine(SetCameraPositionSmoothingActive());
    }

    private IEnumerator SetCameraPositionSmoothingActive()
    {
        //Debug.Log("Activate camera smoothing");
        cameraPositionSmoothingActive = true;
        yield return new WaitForSecondsRealtime(1.5f);
        cameraPositionSmoothingActive = false;
        //Debug.Log("Deactivate camera smoothing");
    }

    private void FixedUpdate()
    {
        if (!isCurrentlyIgniting)
        {
            ModifyTemperatureBy(-Time.fixedDeltaTime * 1.0f);
        }

        if (_isPlayerDead && !_uiManager.IsOverlayActive() && !_transitioning)
        {
            _uiManager.SetRestartOverlay(true);
        }
    }

    #region Stroke

    // Returns whether the user can still input
    public bool IsAnotherStrokeAvailable()
    {
        return _currentStroke < numberOfMainStrokes + numberOfCorrectionStrokes;
    }

    // Returns the power of the current stroke
    public float GetCurrentStrokeMultiplier()
    {
        return (_currentStroke < numberOfMainStrokes) ? 1f : correctionStrokeMultiplier;
    }

    public bool IsNextStrokeAMainStroke()
    {
        return _currentStroke < numberOfMainStrokes;
    }

    public float ResetCurrentStrokeCount()
    {
        MainStrokeIcon.SetActive(true); // move this also to the ui manager
        _uiManager.UpdateMinorStrokeCount(numberOfCorrectionStrokes);

        return _currentStroke = 0;
    }

    public float IncreaseCurrentStrokeCount()
    {
        _currentStroke++;

        if (_currentStroke == 1)
        {
            MainStrokeIcon.SetActive(false);
        }

        _uiManager.UpdateMinorStrokeCount(numberOfCorrectionStrokes - _currentStroke + 1);

        return _currentStroke;
    }

    #endregion Stroke

    public static bool IsPlayerDead()
    {
        return _isPlayerDead;
    }

    public static bool IsGoalAchieved()
    {
        return _goalAchieved;
    }

    private void LevelGoalAchieved()
    {
        _goalAchieved = true;
        
        if (!_uiManager.IsOverlayActive() && !_transitioning)
        {
            var lastUnlockedLevelNumber = GlobalGameSettings.GetLastUnlockedLevelNumber();
            if (lastUnlockedLevelNumber < GlobalGameSettings.LEVEL_LIST.Count-1 && GlobalGameSettings.LEVEL_LIST[lastUnlockedLevelNumber] == SceneManager.GetActiveScene().name)
            {
                Debug.Log("Level goal achieved: increasing max unlocked number: " + (lastUnlockedLevelNumber + 1));
                GlobalGameSettings.UpdateLastUnlockedLevelNumber(lastUnlockedLevelNumber);
            }

            _uiManager.SetGoalOverlay(true);
            UpdateTimeScale(0.08f);
        }
    }

    public void UpdateTimeScale(float timeScale)
    {
        Time.timeScale = timeScale;
        Time.maximumDeltaTime = timeScale;
        // Important: always update the fixed time scale for smooth physic calculation
        Time.fixedDeltaTime = timeScale * 0.02f;
    }

    public void PlayerDied()
    {
        if (_goalAchieved || _isPlayerDead) return;

        PlayerParticles.SetActive(false);
        Debug.Log("Player died.");
        AudioManager.instance.Play("Vapor");
        _isPlayerDead = true;
        _playerRigidbody.constraints = RigidbodyConstraints2D.None;
        nextTargetLine.enabled = false;
        nextTargetDot.enabled = false;
        _playerSprite.color = _playerColorLow;
        ResetCurrentStrokeCount();
    }

    public void StartNextLevel()
    {
        Debug.Log("Starting next level.");
        LoadLevel(_currentLevelIndex+1, true);
    }

    public void RestartLevel()
    {
        _isRestarted = true;
        Debug.Log("Restarting level.");
        _uiManager.ClearLevelStartText();
        LoadLevelHelper(-1, false);
    }

    public void LoadLevel(int levelIndex, bool showTextTransition)
    {
        LoadLevelHelper(levelIndex, showTextTransition);
    }

    // If level number == -1: restart level
    private void LoadLevelHelper(int levelNumber, bool showTextTransition)
    {
        _transitioning = true;
        _uiManager.SetRestartOverlay(false);
        _uiManager.SetGoalOverlay(false);

        transition.SetTrigger("Start");

        // TODO: Interfere here and only execute the final steps after all dialogs are shown
        Time.timeScale = 1f;
        levelToLoad = levelNumber;

        // Update the transition text and initiate the ExecuteLoadLevelLogic trigger
        if (levelNumber != -1)
        {
            _uiManager.SetFirstIntroText();
        }
        else
        {
            StartCoroutine(_uiManager.EndOfIntroTextsReached(1f));
        }
    }

    public void ExecuteLoadLevelLogic()
    {
        // Restart level if needed
        if (levelToLoad == -1)
        {
            Debug.Log("...restarting scene.");
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        else if (levelToLoad < GlobalGameSettings.LEVEL_LIST.Count)
        {
            var levelName = GlobalGameSettings.LEVEL_LIST[levelToLoad];
            Debug.Log("LoadLevelHelper: Loading level no." + levelToLoad + " and level name " + levelName);
            SceneManager.LoadScene(levelName);
        }
        else
        {
            Debug.Log("...last level finished, showing outro, because level no. is " + levelToLoad);
            SceneManager.LoadScene("OutroScene");
        }
    }

    public IEnumerator ActivateAutoAim(float seconds)
    {
        var targetValue = 15f;
        var steps = 10;
        for (var i = 0f; i < 1; i += 1f/steps)
        {
            AutoCorrectionMultiplier = Mathf.Lerp(_initialAutoCorrectionMulitplier, targetValue, i);
            yield return new WaitForSecondsRealtime(seconds/steps);
        } 
        
        Debug.Log("AutoAim disabled");
        AutoCorrectionMultiplier = _initialAutoCorrectionMulitplier;
    }

    public IEnumerator ChangePlayerFlameColor(float seconds, Color color)
    {
        var colorModule = PlayerParticles.GetComponent<ParticleSystem>().colorOverLifetime;
        
        var newGradient = new Gradient();
        newGradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(color, 0.0f), new GradientColorKey(Color.gray, 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(0.8f, 0.0f), new GradientAlphaKey(0.1f, 1.0f) }
        );
        colorModule.color = newGradient;
        yield return new WaitForSecondsRealtime(seconds);
        colorModule.color = _playerParticleGradient;
    }


    #region Game State

    //TODO move to a better place within project
    public void ModifyTemperatureBy(float temperature)
    {
        currentTemp += temperature;

        // Change the player color
        if (currentTemp < WARNING_TEMPERATURE) {
            _playerSprite.color = Color.Lerp(_playerSprite.color, _playerColorLow, 0.2f * Time.deltaTime);
            _uiManager.EnableLowTemperatureText(true);
        } else {
            _playerSprite.color = Color.Lerp(_playerSprite.color, Color.white, 0.1f * Time.deltaTime);
            _uiManager.EnableLowTemperatureText(false);
        }

        // Switch animation and adjust mass
        for (var i = 2; i >= 0; i--)
        {
            if (currentTemp >= _tempSideEffectsValues[i].temperature)
            {
                StartTempSideEffects(i);
                break;
            }
        }

        if (Mathf.Abs(temperature) > EFFECT_THRESHOLD)
        {
            PlayEffect(temperature > 0);
        }
    }

    private void StartTempSideEffects(int level)
    {
        if (_lastCachedLevel == level) return;
        _lastCachedLevel = level;

        var sideEffects = _tempSideEffectsValues[level];
        PlayEffect(playerAnimator.GetInteger(ANIMATION_STATE_LEVEL_TAG) < level);
        playerAnimator.SetInteger(ANIMATION_STATE_LEVEL_TAG, sideEffects.animationStateLevel);
        _playerRigidbody.mass = sideEffects.mass;
    }

    #endregion

    //TODO move to correct position in project
    private void PlayEffect(bool isHeatingUp)
    {
        // Heat
        if (isHeatingUp && heatEffect != null)
        {
            ApplyOnEffect(Instantiate(heatEffect));
        }
        // Cold
        else if (coldEffect != null)
        {
            ApplyOnEffect(Instantiate(coldEffect));
        }

        void ApplyOnEffect(ParticleSystem effectInstance)
        {
            effectInstance.transform.parent = Player.transform;
            effectInstance.transform.position = Player.transform.position;
            effectInstance.gameObject.AddComponent<Destroy>();
            effectInstance.GetComponent<Destroy>().DestroyOverTime(1);
        }
    }

    #region Debug
    public void ToggleAmbientParticles()
    {
        Debug.Log(ambientParticles.gameObject.activeSelf, ambientParticles);
        ambientParticles.gameObject.SetActive(!ambientParticles.gameObject.activeSelf);
    }

    public void ToggleFPSCounter()
    {
        if (_cachedFPSCounter == null)
            _cachedFPSCounter = GameObject.Find("[Graphy]");
        if (_cachedFPSCounter)
        {
            _cachedFPSCounter.SetActive(!_cachedFPSCounter.activeSelf);
        } else
        {
            Debug.LogError("No FPS counter initialized.");
        }
    }

    #endregion


    private readonly struct TemperatureMassDefinition
    {
        public readonly float temperature;
        public readonly float mass;
        public readonly int animationStateLevel;
        public readonly Color color;

        public TemperatureMassDefinition(float temperature, float mass, int animationStateLevel, Color color)
        {
            this.temperature = temperature;
            this.mass = mass;
            this.animationStateLevel = animationStateLevel;
            this.color = color;
        }
    }
}